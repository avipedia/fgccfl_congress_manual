$(document).ready(function() {
    
    $('.gf').hide(1);
    $('#gfOn').hide(1);
    
    var gfToggle = function() {
        $('.notgf').toggle(1);
        $('.gf').toggle(1);
        $('#gfOn').toggle(1);
        $('#gfOff').toggle(1);
    };
    
    $('#gfOn').click(gfToggle);
    $('#gfOff').click(gfToggle);
        
    $('body').scrollspy({ target: '#manual-parts' });
    
});